all:
	gcc -std=c11 -pedantic -Wall main.c -O3 -o unitprop

debug:
	gcc -std=c11 -pedantic -Wall main.c -ggdb -D DONT_INLINE -o unitprop

valgrind:
	gcc -std=c11 -pedantic -Wall main.c -g -D DONT_INLINE -O0 -o unitprop
