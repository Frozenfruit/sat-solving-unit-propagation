## About
This is a piece of code for an assignment i have to do for a lecture called 'Practical SAT solving'.
The goal of this assignment is to implement a program that takes a CNF in DIMACS format, does one step of
unit propagation and outputs the reduced CNF.

## Compiling
Just run make
