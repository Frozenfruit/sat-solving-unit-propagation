#define _XOPEN_SOURCE 800
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdint.h>

#define true 1
#define false 0

//#define VERBOSE
//#define DONT_INLINE

#ifdef DONT_INLINE
    #define INLINE
#else
    #define INLINE inline
#endif
//global variables
//the file pointer for the iput file
FILE* input;

//path to the input file
const char* filename;

//number of variables in the input
int numVariables;

//number of clauses in the input
int numClauses;

//number of clauses that where read in
int curClause = 0;

//list of clauses. this is an array of size numClauses of pointers to integer arrays representing clauses.
//each clause is a integer array where the first entry contains the size of the clause and index 1 the number of
//unasigned variables
int32_t** cnf;

//occurences list. For each variable list all clauses that contain that variable
//implemented as an array of arrays of indexes into the cnf array
//again index 0 of each array contains the number of elements
//index 1 contains the length of the underlying array
//uint32_t** occurrencesList;

//the two watched literals data structure
//implemented as an array of pointers into arrays that contain the number of literal-pointer pairs
//as the first element, the size of the array in pairs as the second element and then literal-pointer pairs
//In these pairs the other watched literal comes first and than an indec into the clause array pointing to the
//watched clause. If the clause is a two-clause this index is -1
//The length of the array is 2 * numVariables. The negated literals come first
int32_t** twoWatchedLiterals;

//the stack that contains all unit clauses
//first element is the number of units in the array
//second element is the maximum number of units the array can hold
int32_t* units;

//partial assignment
//each byte corresponds to a variable
//0 = unasigned
//-1 = false assigned
//1 = true assigned
int8_t* partialAssignment;

//struct representing one clause
//TODO: change this to a simple pointer array and interpret the first entry as the size
//      This saves on one indirection
//struct Clause {
//    int size;
//    int* literals;
//};

//list of functions
void fileopen();
int parseInt(int start, const char* line, int* end);
INLINE int skipWhitespace(int start, char* str);
void parsePreamble(char* line);
void parseFile();
INLINE void resizeClauseBuffer(int lineSize);
void initUnits();
void initTwoWatchedLiterals();
//void initOccurrencesList();
void initPartialAssignment();
INLINE uint32_t getIndexToTwoWatchedLiterals(int32_t literal);
INLINE void watchClause(uint32_t clause, int32_t literal, int32_t other);
void propagateUnit(int32_t unit);
int32_t popUnit();
void pushUnit(int32_t unit);

//debug methods
void printClauses();
void printUnits();
void printTwoWatchedLiterals();
void printPartialAssignment();
void printCNF();

int main( int argc, const char* argv[] )
{
    //no input file given
    if (argc == 1) {
        printf("WARNING: need the filename as first argument! shutting down.\n");
        exit(1);
    }

    filename = argv[1];
    printf("c filename: %s\n", filename);
    fileopen();
    initUnits();
    parseFile();

#ifdef VERBOSE
    printClauses();
    printUnits();
#endif

    //printf("curClause: %d, numClauses: %d\n", curClause, numClauses);
    initTwoWatchedLiterals();

#ifdef VERBOSE
    printTwoWatchedLiterals();
    printPartialAssignment();
#endif

    //init done, now we can start with unit propagation
    while (units[0]) {
        propagateUnit(popUnit());

#ifdef VERBOSE
        printTwoWatchedLiterals();
        printPartialAssignment();
        printUnits();
#endif
    }

    printCNF();
}

/*void addToOccurrencesList(int32_t literal) {
    int sizeOccurrencesList
}

void initOccurrencesList() {
    occurrencesList = malloc(sizeof(uint32_t*) * numVariables);
    for (int i = 0; i < numVariables; i++) {
        occurrencesList[i] = malloc(sizeof(uint32_t) * 10);
    }
    //fill the array
    for (int i = 0; i < numClauses; i++) {
        for (int j = 2; j < cnf[i][0] + 2; j++) {

        }
    }
}*/

INLINE void removeWatchedClause(uint32_t watchedLiteralIndex, uint32_t watchedClauseIndexToRemove) {
    /*if(watchedLiteralIndex + 1 == 85) {
        printf("removing clause %d from 85 (other literal was %d)\n", twoWatchedLiterals[watchedLiteralIndex][watchedClauseIndexToRemove], twoWatchedLiterals[watchedLiteralIndex][watchedClauseIndexToRemove+1]);
    }*/
    for (int i = watchedClauseIndexToRemove; i < twoWatchedLiterals[watchedLiteralIndex][0]*2; i++) {
        twoWatchedLiterals[watchedLiteralIndex][i] = twoWatchedLiterals[watchedLiteralIndex][i+2];
    }
    twoWatchedLiterals[watchedLiteralIndex][0] -= 1;
}

//TODO: optimize such that satisfied clauses have satisfied variables watched
void propagateUnit(int32_t unit) {
#ifdef VERBOSE
    printf("c \nc \nc propagating unit %d:\n", unit);
#else
    printf("c propagating unit %d:\n", unit);
#endif
    uint32_t index = getIndexToTwoWatchedLiterals(-unit);
    /*if (unit == -85) {
            //printTwoWatchedLiterals();
            printf("number of pairs in the watched literal data structure: %d\n", twoWatchedLiterals[index][0]);
            for (int i = 2; i < twoWatchedLiterals[index][0] * 2 + 2; i += 2) {
                    printf("(i: %d, other: %d, clause: %d), ", i, twoWatchedLiterals[index][i], twoWatchedLiterals[index][i+1]);
            }
            printf("\n");
    }*/
    //loop over all watched clauses
    for (int i = 2; i < twoWatchedLiterals[index][0] * 2 + 2; i += 2) {
        //the other watched literal should not already satisfy the clause
        int32_t other = twoWatchedLiterals[index][i];
        //printf("the other literal: %d (i = %d), number of elements int the array: %d\n", other, i, twoWatchedLiterals[index][0]);
        if ( !( (other < 0 && partialAssignment[abs(other)-1] == -1) || (other > 0 && partialAssignment[abs(other)-1] == 1) ) ) {
            //printf("the other %d literal does not satisfy the clause\n", other);
            //three possibilities:
            //1) the clause is satisfied by another literal in the clause
            //2) it is not and we need to find a new literal to watch
            //3) the clause becomes unit
            int isSatisfied = 0;
            //case 1) first:
            int32_t unasigned = 0;
            int numUnasigned = 0;
            //printf("1)twoWatchedLiterals[index][i+1]: %d\n", twoWatchedLiterals[index][i+1]);
            if (twoWatchedLiterals[index][i+1] > -1) {
                int32_t* clause =  cnf[twoWatchedLiterals[index][i+1]];
                //printf("complete clause: ");
                for (int j = 2; j < clause[0] + 2; j++) {
                    int32_t literal = clause[j];
                    //printf("%d ", literal);
                    if ( (literal < 0 && partialAssignment[abs(literal)-1] == -1) || (literal > 0 && partialAssignment[abs(literal)-1] == 1) ) {
                        //clause is satisfied, nothing to do here, thats case 1)
                        //printf("clause is satisfied!\n");
                        isSatisfied = 1;
                        break;
                    } else if (partialAssignment[abs(literal)-1] == 0) {
                        unasigned = literal;
                        numUnasigned++;
                    }
                }
            }
            //its not case 1
            if (!isSatisfied) {
                //printf("\nthe caluse is not satisfied\n");
                int isUnit = 0;
                //case 3)
                if (numUnasigned == 1 || twoWatchedLiterals[index][i+1] == -1) {
                    //printf("c The clause only contains two literals, %d is unit now\n", other);
#ifdef VERBOSE
                    printf("c The clause only contains two literals, %d is unit now\n", other);
#endif
                    //the other literal becomes unit, add to partial assignment and unit propagation stack
                    partialAssignment[abs(other) - 1] = other > 0 ? 1 : -1;
                    pushUnit(other);
                    isUnit = 1;
                //search for a new literal to watch
                }

                //its not case 3)
                if(!isUnit) {
                    //printf("the new watched literal for this clause is %d\n", unasigned);
                    //printf("2)twoWatchedLiterals[index][i+1]: %d\n", twoWatchedLiterals[index][i+1]);
                    //okay, so the clause is not satisfied and there are more than two literals
                    //unasigned must become the new watched literal, this and others data has to be updated
                    uint32_t otherIndex = getIndexToTwoWatchedLiterals(other);
                    int k = 2;
                    //search for the others literal entry
                    //must have this unit as the other literal and the index into the clause array must be the same
                    //printf("unit: %d ", -unit);
                    while(!(twoWatchedLiterals[otherIndex][k] == -unit && twoWatchedLiterals[otherIndex][k+1] == twoWatchedLiterals[index][i+1])) {
                        /*if (k < twoWatchedLiterals[otherIndex][0] * 2 + 4 && twoWatchedLiterals[otherIndex][k] == -unit) {
                            printf("K: %d\n", k);
                            printf("number of pairs in twoWatchedLiterals[otherIndex]: %d\n", twoWatchedLiterals[otherIndex][0]);
                            printf("twoWatchedLiterals[otherIndex][k+1]: %d, twoWatchedLiterals[index][i+1]: %d\n", twoWatchedLiterals[otherIndex][k+1], twoWatchedLiterals[index][i+1]);
                        }*/
                        k += 2;
                    }
                    //printf("3)twoWatchedLiterals[index][i+1]: %d\n", twoWatchedLiterals[index][i+1]);
                    twoWatchedLiterals[otherIndex][k] = unasigned;
                    //printf("4)twoWatchedLiterals[index][i+1]: %d\n", twoWatchedLiterals[index][i+1]);
                    //add an entry to the watched list of unasigned
                    watchClause(twoWatchedLiterals[index][i+1], unasigned, other);
                    //remove this clasue from the watched list
                    removeWatchedClause(index, i);
                    //after deletition there is a different entry at this index!
                    i = i -2;
#ifdef VERBOSE
                    printf("c The Clause %d is not satisfied. %d becomes the new watched literal.\n", twoWatchedLiterals[index][i+1], unasigned);
#endif
                }
            }
        } else {
#ifdef VERBOSE
            printf("c the other literal %d satisfies the clause %d\n", other, twoWatchedLiterals[index][i+1]);
#endif
        }
    }
}

void initPartialAssignment() {
    //printf("malloced the partial assignment with a size of %d variables\n", numVariables);
    partialAssignment = malloc(sizeof(int8_t) * numVariables);
    for (int i = 0; i < numVariables; i++) {
        partialAssignment[i] = 0;
    }
}

//calculate the index into the twoWatchedLiterals array
INLINE uint32_t getIndexToTwoWatchedLiterals(int32_t literal) {
    uint32_t index = 0;
    if (literal > 0) {
        index = numVariables + literal - 1;
    } else {
        index = -1 * literal - 1;
    }
    return index;
}

INLINE void watchClause(uint32_t clause, int32_t literal, int32_t other) {
    uint32_t index = getIndexToTwoWatchedLiterals(literal);
    //number of pairs
    //printf("literal to watch: %d. Calculated index: %d\n", literal, index);
    uint32_t numPairs = twoWatchedLiterals[index][0];
    //size of array in pairs
    uint32_t arrayLen = twoWatchedLiterals[index][1];
    //is there enough space in the array left?
    if (numPairs == arrayLen) {
        //we need a new array
        int32_t* newArray = malloc(sizeof(int32_t) * (arrayLen * 2 * 2 + 2));
        //dest, source, size
        memcpy(newArray, twoWatchedLiterals[index], sizeof(int32_t) * (arrayLen * 2 + 2));
        newArray[1] = arrayLen * 2;
        free(twoWatchedLiterals[index]);
        twoWatchedLiterals[index] = newArray;
    }
    //add the new entry
    twoWatchedLiterals[index][numPairs * 2 + 2] = other;
    //is this a clause with two literals?
    //printf("clause: %d\n", clause);
    if (cnf[clause][0] == 2) {
        twoWatchedLiterals[index][(numPairs * 2 + 2) + 1] = -1;
    } else {
        twoWatchedLiterals[index][(numPairs * 2 + 2) + 1] = clause;
    }
    //update the number of pairs
    twoWatchedLiterals[index][0] += 1;
}

void initTwoWatchedLiterals() {
    //literal can occour positive and negative
    twoWatchedLiterals = malloc(sizeof(int32_t*) * numVariables * 2);
    //init to some fixed length:
    for(int i = 0; i < 2 * numVariables; i++) {
        twoWatchedLiterals[i] = malloc(sizeof(int32_t) * 20);
        twoWatchedLiterals[i][0] = 0;
        twoWatchedLiterals[i][1] = 9;
    }
    //for each clause watch the first two literals
    for (int i = 0; i < numClauses; i++) {
        //printf("i: %d, size of the clause: %d\n", i, cnf[i][0]);
        int32_t firstLiteral = cnf[i][2];
        int32_t secondLiteral = cnf[i][3];
        /*if (secondLiteral == 85) {
            printf("adding clause %d to the watchedLiteral list of 85, first literal: %d\n", i, firstLiteral);
        }*/
        watchClause(i, firstLiteral, secondLiteral);
        watchClause(i, secondLiteral, firstLiteral);
    }
}

int32_t popUnit() {
    units[0] -= 1;
    return units[units[0] + 2];
}

void pushUnit(int32_t unit) {
    //number of units in the array
    uint32_t numUnits = units[0];
    //max number of units the array can hold
    uint32_t maxUnits = units[1];

    //test if the unit conflicts with another unit:
    for (int i = 0; i < numUnits; i++) {
        if (units[i] == -unit) {
            printf("c Formular is unsat!\n");
            printf("p cnf 0 1\n");
            exit(0);
        }
    }

    //is there room for another unit?
    if (numUnits == maxUnits) {
        //we need a new array
        int32_t* newArray = malloc(sizeof(int32_t) * (maxUnits * 2 + 2));
        //dest, source, size
        memcpy(newArray, units, sizeof(int32_t) * (maxUnits + 2));
        newArray[1] = maxUnits * 2;
        free(units);
        units = newArray;
    }
    //add the new uint to the stack
    units[numUnits + 2] = unit;
    units[0] += 1;
}

void initUnits() {
    units = malloc(sizeof(int32_t) * 50);
    units[0] = 0;
    units[1] = 48;
}

INLINE int32_t abs(int32_t a) {
    if (a > 0) {
        return a;
    }
    return -a;
}

int* clauseBuffer = NULL;
int clauseBufferSize = 20;
INLINE void resizeClauseBuffer(int lineSize) {
    if (lineSize > clauseBufferSize) {
        clauseBufferSize = lineSize;
        clauseBuffer = realloc(clauseBuffer, clauseBufferSize*sizeof(int));
    }
}

//parse clause and add to list of clauses
void parseClause(char* line, int size) {
    //printf("parsing clause %d. size: %d\n", curClause, size);
    //printf("clause string: %s\n", line);
    skipWhitespace(0, line);
    if ( (!isdigit(line[0]) || line[0] == '0') && line[0] != '-') {
        printf("c WARNING: this line is not a valid clause, ignoring it: %s", line);
        numClauses--;
        return;
    }
    //number of integers in the line including the trailing 0
    int numLiterals = 0;
    resizeClauseBuffer(size);
    int pointer = 0;
    while(line[pointer] != '\n') {
        clauseBuffer[numLiterals] = parseInt(pointer, line, &pointer);
        //printf("read int: %d\n", clauseBuffer[numLiterals]);
        numLiterals++;
        //printf("pointer: %d\n", pointer);
        pointer = skipWhitespace(pointer, line);
        //printf("token: %d\n", line[pointer]);
    }

    if (numLiterals - 1 == 1) {
        pushUnit(clauseBuffer[0]);
        //printf("index: %d \n", abs(clauseBuffer[0])-1);
        if (clauseBuffer[0] < 0) {
            partialAssignment[abs(clauseBuffer[0])-1] = -1;
        } else {
            partialAssignment[abs(clauseBuffer[0])-1] = 1;
        }
        numClauses--;
        return;
    }

    //numLiterals includes the 0 at the end of the line
    //+2 because we want to store the size of the array as its first element
    //and the number of unasigned literals at index 2
    //printf("numLiterals: %d\n", numLiterals);
    int32_t* literalArray = malloc((numLiterals - 1 + 2) * sizeof(int));
    //dest, source, size
    memcpy(&literalArray[2], clauseBuffer, (numLiterals-1)*sizeof(int));
    /*for (int i = 0; i < numLiterals - 1; i++) {
        literalArray[i+2] = clauseBuffer[i];
    }*/
    literalArray[0] = numLiterals - 1;
    //at the beginning all literals are unasigned
    literalArray[1] = numLiterals - 1;
    cnf[curClause] = literalArray;
    //printf("first number in current clause: %d\n", clause->literals[0]);
    curClause++;
}

//parse the file
void parseFile() {
    //http://stackoverflow.com/questions/3501338/c-read-file-line-by-line
    char * line = NULL;
    size_t len = 0;
    size_t read;
    clauseBuffer = malloc(clauseBufferSize * sizeof(int));

    while ((read = getline(&line, &len, input)) != -1) {
        if (line[0] == 'p') {
            printf("c found preamble: %s", line);
            parsePreamble(line);
            initPartialAssignment();
        }
        if (isdigit(line[0]) || line[0] == '-') {
            //printf("next line: %s", line);
            if (line[0] == '0') {
                printf("WARNING: clause begins with 0! shutting down.");
                exit(1);
            }
            parseClause(line, len);
        }
    }

    //free(clauseBuffer);
    //fclose(input);
}

//parse the line looking like "p cnf x y"
void parsePreamble(char* line) {
    int pointer = 1;
    pointer = skipWhitespace(pointer, line);
    //cnf
    if (!(line[pointer] == 'c' && line[++pointer] == 'n' && line[++pointer] == 'f')) {
        printf("WARNING something is wrong with the preamble!\n");
        exit(1);
    }
    pointer = skipWhitespace(++pointer, line);
    //printf("the line before the variable num parse: %s\n", (line+pointer));
    numVariables = parseInt(pointer, line, &pointer);
    printf("c number of variables: %d\n", numVariables);
    pointer = skipWhitespace(pointer, line);
    numClauses = parseInt(pointer, line, &pointer);
    printf("c number of clauses: %d\n", numClauses);
    //init the clause array
    cnf = malloc(sizeof(uint32_t*) * numClauses);
}

//skip to the next non-space token
INLINE int skipWhitespace(int start, char* str) {
    while(str[start] == ' ') {
        start++;
    }
    return start;
}

//assumes that start points to the first character of an integer in line.
//parses the integer and returns the index of the first char after the integer that was read.
int32_t parseInt(int start, const char* line, int* end) {
    int8_t sign = 1;
    uint32_t value = 0;
    if (line[start] == '-') {
        sign = -1;
        start++;
    }
    while(isdigit(line[start])) {
        value = value*10 + (line[start]-48);
        start++;
    }
    *end = start;
    return value*sign;
}

//open the file or return with an error
void fileopen() {
    input = fopen(filename, "r");
    if (input == NULL) {
        printf("c filename '%s' is not a valid file!\n", filename);
        exit(1);
    }
}

void printClauses() {
    for (int i = 0; i < numClauses; i++) {
        printf("c size of clause %d: %d. Clause: (", i, cnf[i][0]);
        for (int j = 2; j < cnf[i][0] + 2; j++) {
            printf("%d ", cnf[i][j]);
        }
        printf(")\n");
    }
}

void printUnits() {
    printf("c current stack of unit clauses: ");
    for (int i = 2; i < units[0] + 2; i++) {
        printf("%d ", units[i]);
    }
    printf("\n");
}

void printTwoWatchedLiterals() {
    for (int i = 0; i < numVariables * 2; i++) {
        if (i < numVariables) {
            printf("c Watched by %d: ", -(i+1));
        } else {
            printf("c Watched by %d: ", (i - numVariables) + 1);
        }
        for (int j = 2; j < twoWatchedLiterals[i][0] * 2 + 2; j += 2) {
            printf("(other: %d, clause: %d), ", twoWatchedLiterals[i][j], twoWatchedLiterals[i][j+1]);
        }
        printf("\n");
    }
}

void printPartialAssignment() {
    printf("c partial assignment: ");
    for (int i = 0; i < numVariables; i++) {
        if (partialAssignment[i] != 0) {
            printf("%d ", (i + 1) * partialAssignment[i]);
        }
    }
    printf("\n");
}

void printCNF() {
    //calculate the number of clauses that are left
    int numClausesLeft = 0;
    for (int i = 0; i < numClauses; i++) {
        int32_t* clause = cnf[i];
        clause[1] = 1;
        for (int j = 2; j < clause[0] + 2; j++) {
            int32_t literal = clause[j];
            if ( (literal < 0 && partialAssignment[abs(literal)-1] == -1) || (literal > 0 && partialAssignment[abs(literal)-1] == 1) ) {
                //clause is satisfied, nothing to do here
                //printf("clause %d is satisfied\n", i);
                clause[1] = 0;
                break;
            }
        }
        if (clause[1] == 1) {
            numClausesLeft++;
        }
    }

    //we need to add all units as well
    for (int i = 0; i < numVariables; i++) {
        if (partialAssignment[i] != 0) {
            numClausesLeft++;
        }
    }

    //print the remaining formular
    printf("p cnf %d %d\n", numVariables, numClausesLeft);
    for (int i = 0; i < numClauses; i++) {
        int32_t* clause = cnf[i];
        if (clause[1] == 1) {
            for (int j = 2; j < clause[0] + 2; j++) {
                int32_t literal = clause[j];
                //only print the literals that are not asigned:
                if (partialAssignment[abs(literal)-1] == 0) {
                    printf("%d ", literal);
                }
            }
            printf("0\n");
        }
    }

    //write the unit clauses
    for (int i = 0; i < numVariables; i++) {
        if (partialAssignment[i] != 0) {
                printf("%d 0\n", partialAssignment[i] * (i+1));
        }
    }
}
